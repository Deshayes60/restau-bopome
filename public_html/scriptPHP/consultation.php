<?php

session_start();
include'../connexion.php';
$msg = "";

if (isset($_POST['idConsultationMet'])) {
    $id = $_POST['idConsultationMet'];
    $stmt = $db->query('SELECT met.id AS idMet,met.nom AS Met, ingredient.libelle AS Ingrédient, ingredient.id AS idIngre, round(quantite) AS quantite, typeunite.libelle AS unite , round(prixNombreUnite * quantite/nombreUnite,3) AS prix FROM metingredient 
join met on metingredient.idmet = met.id 
join ingredient on metingredient.idingredient = ingredient.id 
join typeunite on ingredient.idTypeUnite = typeunite.id
WHERE idMet = ' . $id);
    $stmt->execute();
    $valeur = $stmt->fetchAll();
    echo json_encode($valeur);
    die();
}
if(isset($_POST['date'])){
    $stmt= $db->prepare('SELECT met.nom as nom , met.id as idMet , quantiteVendue as nbVente FROM vente
JOIN met on met.id = vente.idMet
WHERE date = :date');
    $stmt->execute(array('date' => $_POST['date']));
    $vente = $stmt->fetchAll();
    echo json_encode($vente);
    die();
}
if (isset($_POST['consultationIngredient'])) {
    $stmt = $db->query('SELECT ingredient.id as idIngredient, ingredient.libelle AS nom, typeunite.libelle AS unite FROM ingredient JOIN typeunite on typeunite.id = ingredient.idtypeunite ORDER BY nom ASC');
    $valeur = $stmt->fetchAll();
    echo json_encode($valeur);
    die();
}
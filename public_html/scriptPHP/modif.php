<?php

session_start();
include'../connexion.php';
$msg = "";

if (isset($_POST['idMet'])) {
    $stmt = $db->query('SELECT * FROM met WHERE id = ' . $_POST['idMet']);
    $stmt->execute();
    $valeur = $stmt->fetch();
    echo json_encode($valeur);
    die();
}

if (isset($_POST['idIngredient'])) {
    $stmt = $db->query('SELECT * FROM ingredient WHERE id = ' . $_POST['idIngredient']);
    $stmt->execute();
    $valeur = $stmt->fetch();
    echo json_encode($valeur);
    die();
}

if (isset($_POST['modifMet'])) {
    $nom = $_POST['nomMet'];
    $description = $_POST['descriptionMet'];
    $com = $_POST['comMet'];
    $type = $_POST['typeMet'];
    if ($nom == "") {
        $msg = "Veuillez saisir un nom de met";
        echo json_encode($msg);
        header("location: ../index.php#modifMet");
    }
    if ($description == "") {
        $msg = "Veuillez saisir une description de met";
        echo json_encode($msg);
        header("location: ../index.php#modifMet");
    }
    if ($type == -1) {
        $msg = "Veuillez saisir un type de met";
        echo json_encode($msg);
        header("location: ../index.php#modifMet");
    }
    if ($msg == "") {
        $stmt = $db->prepare('UPDATE met SET nom = :nom, description = :descr, commentaire = :com, idTypeMet = :type WHERE id = :id ');
        $ret = $stmt->execute(array('nom' => $nom, 'descr' => $description, "com" => $com, "type" => $type, "id" => $_POST['modifIdMet']));
        if ($ret) {
            $msg = "Modification réussie";
            $_SESSION['modifMet'] = $msg;
            header("location: ../index.php#mets");
        } else {
            $msg = 'La modification a échoué à échoué';
            echo json_encode($msg);
            header("location: ../index.php#modifMet");
        }
    }
}

if (isset($_POST['modifIngredient'])) {
    $libelle = $_POST['libelleModifIngredient'];
    $nbUnit = $_POST['modifNbUnite'];
    $prix = $_POST['modifPrixUnite'];
    $type = $_POST['modifTypeUnite'];
//    if ($nom == "") {
//        $msg = "Veuillez saisir un nom de met";
//        echo json_encode($msg);
//        header("location: ../index.php#modifMet");
//    }
//    if ($description == "") {
//        $msg = "Veuillez saisir une description de met";
//        echo json_encode($msg);
//        header("location: ../index.php#modifMet");
//    }
//    if ($type == -1) {
//        $msg = "Veuillez saisir un type de met";
//        echo json_encode($msg);
//        header("location: ../index.php#modifMet");
//    }
    $stmt = $db->prepare('UPDATE ingredient SET libelle = :libelle, nombreUnite = :nbUnit, prixNombreUnite = :prix, idTypeUnite = :type WHERE id = :id ');
    $ret = $stmt->execute(array('libelle' => $libelle, 'nbUnit' => $nbUnit, 'prix' => $prix, "type" => $type, "id" => $_POST['modifIdIngredient']));
    if ($ret) {
        $msg = "Modification réussie";
        $_SESSION['modifMet'] = $msg;
        header("location: ../index.php#ingredient");
    } else {
        $msg = 'La modification a échoué à échoué';
        echo json_encode($msg);
        header("location: ../index.php#modifIngredient");
    }
}
if (isset($_POST['modifCompoMet'])) {
    $stmt = $db->prepare('INSERT INTO metingredient VALUES(:idMet, :idIngre,:qte)');
    $ret = $stmt->execute(array('idMet' => $_POST['idMetCompo'], 'idIngre' => $_POST['idIngreCompo'], 'qte' => $_POST['qte']));
     if ($ret) {
        $msg = "Ajout réussi";
        echo json_encode($msg);
        die();
    } else {
        $msg = 'L\'ajout a échoué ';
        echo json_encode($msg);
        die();
    }
}
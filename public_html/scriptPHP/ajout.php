<?php
include'../connexion.php';
$msg = "";

if (isset($_POST['ajoutMet'])) {
    $nom = $_POST['nomMet'];
    $description = $_POST['descriptionMet'];
    $com = $_POST['comMet'];
    $type = $_POST['typeMet'];
    if ($nom == "") {
        $msg = "Veuillez saisir un nom de met";
        echo json_encode($msg);
        header("location: ../index.php#ajoutMet");
    }
    if ($description == "") {
        $msg = "Veuillez saisir une description de met";
        echo json_encode($msg);
        header("location: ../index.php#ajoutMet");
    }
    if ($type == -1) {
        $msg = "Veuillez saisir un type de met";
        echo json_encode($msg);
        header("location: ../index.php#ajoutMet");
    }
    if ($msg == "") {
        $stmt = $db->prepare('INSERT INTO met (nom, description, commentaire, idTypeMet) VALUES(:nom, :descr, :com, :type)');
        $ret = $stmt->execute(array('nom' => $nom, 'descr' => $description, "com" => $com, "type" => $type));
        if ($ret) {
            $msg = "Ajout réussi";
            echo json_encode($msg);
            header("location: ../index.php#mets");
        } else {
            $msg = 'L\'ajout à échoué';
            echo json_encode($msg);
            header("location: ../index.php#ajoutMet");
        }
    }
};
if (isset($_POST['ajoutIngredient'])) {
    $libelle = $_POST['libelleIngredient'];
    $nbUnit = $_POST['nbUnite'];
    $prix = $_POST['prixUnite'];
    $type = $_POST['typeUnite'];
    if ($msg == "") {
        $stmt = $db->prepare('INSERT INTO ingredient (libelle, nombreUnite, prixNombreUnite, idTypeUnite) VALUES(:libelle,:nbUnit, :prix, :type)');
        $ret = $stmt->execute(array('libelle'=>$libelle, 'nbUnit'=>$nbUnit, 'prix'=>$prix, "type" => $type));
        if ($ret) {
            $msg = "Ajout réussi";
            echo json_encode($msg);
            header("location: ../index.php#ingredient");
        } else {
            $msg = 'L\'ajout à échoué';
            echo json_encode($msg);
            header("location: ../index.php#ajoutIngredient");
        }
    }
}
?>
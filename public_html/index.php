<?php
session_start();
include'connexion.php';
$lesMets = $db->query('SELECT id, nom from met');
$compoMet = $db->query('SELECT id, nom from met ORDER BY nom ASC');
$lesIngrédient = $db->query('SELECT * FROM ingredient');
$lesPrix = $db->query('select met.nom as met, prixvente.prixDeVente as prix from prixvente join met on met.id = prixvente.idMet');
$lesTypesMets = $db->query('SELECT * FROM typemet');
$typemet = $db->query('SELECT * FROM typemet');
$lesTypesUnite = $db->query('SELECT * FROM typeunite');
$typeunit = $db->query('SELECT * FROM typeunite');
?>


<!DOCTYPE html>
<html class="ui-mobile-rendering">
    <head>
        <title>Restaurant Bopome</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <!-- The css -->
        <link rel="stylesheet" href="lib/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css"/>
        <link rel="stylesheet" href="css/css-bopome.css"/>
        <!-- The Scripts -->
        <script src="lib/jquery-1.11.3.min.js"></script>
        <script src="lib/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
    </head>

    <body>
        <!--Page d'accueil-->
        <div data-role="page" id="home">
            <div data-role="header">
                <h1>Restaurant Bopome</h1>
            </div>
            <div data-role="content">
                <ul data-role="listview"  data-inset="true">
                    <li><a href="#gestionCarac">Gestion des caractéristiques</a></li>
                    <li><a href="#gestionVente">Gestion des ventes</a></li>
                    <li><a href="#consultation">Consultation</a></li>
                </ul>
            </div>
        </div>
        <!--Page gestion des caractéristiques-->
        <div data-role="page" id="gestionCarac">
            <div data-role="header">
                <a href="#home" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Gestion des caractéristiques</h1>
            </div>

            <div data-role="content">
                <ul data-role="listview"  data-inset="true">
                    <li><a href="#mets">Mets</a></li>
                    <li><a href="#ingredient">Ingrédients</a></li>
                    <li><a href="#prixVente">Prix de vente</a></li>
                </ul>
            </div>
        </div>
        <!----------------------->
        <!--Page mets-->
        <!----------------------->
        <div data-role="page" id="mets">
            <div data-role="header">
                <a href="#gestionCarac" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Mets</h1>
            </div>

            <div data-role="content">
                <?php foreach ($lesMets as $mets) { ?>
                    <div class="ui-grid-c" id="div-<?php echo $mets->id ?>">
                        <div class="ui-block-a"><span><?php echo $mets->nom ?></span></div>
                        <div class="ui-block-b"><a href="#modifMet" data-role="button" class="modificationMet">Modifier</a></div>
                        <div class="ui-block-c"><a href="#modifCompoMet" data-role="button" class="modifCompositionMet">Modifier la composition</a></div>
                        <div class="ui-block-d"><button id="<?php echo $mets->id ?>" class="suppMet">Supprimer</button></div>
                    </div>
                    <hr>
                <?php } ?>
                <a href="#ajoutMet" data-role="button">Ajout d'un nouveau met</a>
                <a href="#gestionTypeMets" data-role="button">Gestion des types de mets</a>
                <a href="#popupSuppMet" data-rel="popup" data-transition="pop" id="lienPopupSuppMet"></a>
                <center><div data-role="popup" id="popupSuppMet" data-position-to="window">
                        <p style='font-size: 25; color: red;' id='pSuppMet'>Met supprimé</p>
                    </div></center>

                <?php if ($_SESSION['modifMet'] != "") { ?>
                    <a href="#popupModifAccueilMet" data-rel="popup" data-transition="pop" id="lienPopupModifAccueilMet"></a>
                    <center><div data-role="popup" id="popupModifAccueilMet" data-position-to="window">
                            <p style='font-size: 25; color: red;' id='pModifAccueilMet'></p>
                        </div></center>
                    <?php
                    $_SESSION['modifMet'] = "";
                }
                ?>
            </div>
        </div>
<?php ?>
        <!--Page ajout d'un met-->
        <div data-role="page" id="ajoutMet">
            <div data-role="header">
                <a href="#mets" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Ajout d'un met</h1>
            </div>
            <div data-role="content">
                <form method="post" action="scriptPHP/ajout.php" id='ajoutFormMet'  data-ajax="false">
                    <label for="nomMet">Nom du met :</label>
                    <input type="text" name="nomMet" id="nomMet">
                    <label for="nomMet">Description du met :</label>
                    <input type="text" name="descriptionMet" id="descriptionMet">
                    <label for="comMet">Commentaire :</label>
                    <input type="text" name="comMet" id="comMet">
                    <label for="selectType">Type du met :</label>
                    <select name="typeMet" id="select-native-1">
                        <option value='-1'></option>
                        <?php foreach ($lesTypesMets as $type) { ?>
                                                  <option value="<?php echo $type->id ?>"><?php echo $type->libelle ?></option>
<?php } ?>
                                          </select>
                    <input type='submit' name='ajoutMet' value="Ajouter">

                    <div data-role="popup" id="myPopup">
                        <p id='pAjoutMet'></p>
                    </div>
                </form>
            </div>
        </div>
        <!--Page modification d'un met-->
        <div data-role="page" id="modifMet">
            <div data-role="header">
                <a href="#mets" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Modification d'un met</h1>
            </div>
            <div data-role="content">
                <form method="post" action="scriptPHP/modif.php" id='modifFormMet'  data-ajax="false">
                    <label for="nomMet">Nom du met :</label>
                    <input type="text" name="nomMet" id="nomModifMet" value="">
                    <label for="nomMet">Description du met :</label>
                    <input type="text" name="descriptionMet" id="descriptionModifMet">
                    <label for="comMet">Commentaire :</label>
                    <input type="text" name="comMet" id="comModifMet">
                    <label for="selectType">Type du met :</label>
                    <select name="typeMet" id="selectModifMet">
                        <option value='-1'></option>
                        <?php foreach ($typemet as $letype) { ?>
                                                  <option value="<?php echo $letype->id ?>"><?php echo $letype->libelle ?></option>
<?php } ?>
                                          </select>
                    <input type="text"  value='' id='modifIdMet' name='modifIdMet'>
                    <input type='submit' name='modifMet' value="Modifier" id="submitModifMet">

                    <a href="#popupModifMet" data-rel="popup" data-transition="pop" id="lienPopupModifMet"></a>
                    <center><div data-role="popup" id="popupModifMet" data-position-to="window">
                            <p id='pModifMet'></p>
                        </div></center>
                </form>
            </div>
        </div>
        <!--Page composition d'un met-->
        <div data-role="page" id="modifCompoMet">
            <div data-role="header">
                <a href="#mets" data-icon="back" class="back ui-btn-left">Back</a>
                <h1 id="h1compoModifMet"></h1>
            </div>
            <div data-role="content" id="contentCompoModifMet">
                <table data-role="table" id="tableCompoModifMet" data-mode="reflow" class="ui-responsive">
                    <thead>
                        <tr>
                            <th>Ingrédient</th>
                            <th>Quantité</th>
                            <th>Unité</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                <button class='ajoutIngreCompo'>Ajout d'un ingrédient à la composition</button>
            </div>
        </div>
        <!----------------------->
        <!--Page ingrédients----->
        <!----------------------->
        <div data-role="page" id="ingredient">
            <div data-role="header">
                <a href="#gestionCarac" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Ingrédients</h1>
            </div>

            <div data-role="content">
<?php foreach ($lesIngrédient as $ingre) { ?>
                    <div class="ui-grid-b" id="div-<?php echo $ingre->id ?>">
                        <div class="ui-block-a"><span><?php echo $ingre->libelle ?></span></div>
                        <div class="ui-block-b"><a href="#modifIngredient" data-role="button" class="modificationIngredient">Modifier</a></div>
                        <div class="ui-block-c"><button id="<?php echo $ingre->id ?>" class="suppIngre">Supprimer</button></div>
                    </div>
                    <hr>
<?php } ?>
                   
                <a href="#ajoutIngredient" data-role="button">Ajout d'un nouvel ingrédient</a>
            </div>
        </div>
        <!--Page ajout d'un ingrédient-->
        <div data-role="page" id="ajoutIngredient">
            <div data-role="header">
                <a href="#ingredient" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Ajout d'un ingrédient</h1>
            </div>
            <div data-role="content">
                <form method="post" action="scriptPHP/ajout.php" id='ajoutFormIngredient'  data-ajax="false">
                    <label for="libelleIngredient">Libelle de l'ingrédient :</label>
                    <input type="text" name="libelleIngredient" id="libelleIngredient">
                    <label for="nbUnite">Nombre d'unité :</label>
                    <input type="text" name="nbUnite" id="nbUnite">
                    <label for="prixUnite">Prix à l'unité :</label>
                    <input type="text" name="prixUnite" id="prixUnite">
                    <label for="typeUnite">Type d'unité:</label>
                    <select name="typeUnite" id="select-native-1">
                        <option value='-1'></option>
                        <?php foreach ($lesTypesUnite as $typeUnit) { ?>
                                                  <option value="<?php echo $typeUnit->id ?>"><?php echo $typeUnit->libelle ?></option>
<?php } ?>
                                          </select>
                    <input type='submit' name='ajoutIngredient' value="Ajouter">

                    <div data-role="popup" id="myPopup">
                        <p id='pAjoutIngredient'></p>
                    </div>
                </form>
            </div>
        </div>
        <!--Page modification d'un ingrédient-->
        <div data-role="page" id="modifIngredient">
            <div data-role="header">
                <a href="#ingredient" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Modification d'un ingrédient</h1>
            </div>
            <div data-role="content">
                <form method="post" action="scriptPHP/modif.php" id='modifFormMet'  data-ajax="false">
                    <label for="libelleIngredient">Libelle de l'ingrédient :</label>
                    <input type="text" name="libelleModifIngredient" id="libelleModifIngredient">
                    <label for="nbUnite">Nombre d'unité :</label>
                    <input type="text" name="modifNbUnite" id="modifNbUnite">
                    <label for="prixUnite">Prix à l'unité :</label>
                    <input type="text" name="modifPrixUnite" id="modifPrixUnite">
                    <label for="typeUnite">Type d'unité:</label>
                    <select name="modifTypeUnite" id="select-native-1">
                        <option value='-1'></option>
                        <?php foreach ($typeunit as $typeU) { ?>
                                                  <option value="<?php echo $typeU->id ?>"><?php echo $typeU->libelle ?></option>
<?php } ?>
                                          </select>
                    <input type="text"  value='' id='modifIdIngredient' name='modifIdIngredient'>
                    <input type='submit' name='modifIngredient' value="Modifier" id="submitModifIngredient">

                    <a href="#popupModifMet" data-rel="popup" data-transition="pop" id="lienPopupModifMet"></a>
                    <center><div data-role="popup" id="popupModifMet" data-position-to="window">
                            <p id='pModifMet'></p>
                        </div></center>
                </form>
            </div>
        </div>
        <!----------------------->
        <!--Page prix de vente--->
        <!----------------------->
        <div data-role="page" id="prixVente">
            <div data-role="header">
                <a href="#gestionCarac" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Prix de vente</h1>
            </div>

            <div data-role="content">
<?php foreach ($lesPrix as $prix) { ?>
                    <div class="ui-grid-b">
                        <div class="ui-block-a"><span><?php echo $prix->met ?><b> Prix de vente :</b> <?php echo $prix->prix ?><b>€</b></span></div>
                        <div class="ui-block-b"><button>Modifier</button></div>
                        <div class="ui-block-c"><button>Supprimer</button></div>
                    </div>
                    <hr>
<?php } ?>
            </div>
        </div>
        <!----------------------->
        <!--Page gestions des ventes-->
        <!----------------------->
        <div data-role="page" id="gestionVente">
            <div data-role="header">
                <a href="#home" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Gestion des ventes</h1>
            </div>

            <div data-role="content">
                 <select name="select-native-1" id="select-native-1">
                                          <option value="1">Test</option>
                                      </select>
               
                  <div class="ui-grid-b">
                      <div class="ui-block-a"> <a href="#affichageVente" data-role="button" id='ddj-2'>Date du jour -2</a></div>
                   <div class="ui-block-b"> <a href="#affichageVente" data-role="button" id='ddj-1'>Date du jour -1</a></div>
                  <div class="ui-block-c">  <a href="#affichageVente" data-role="button" id='ddj'>Date du jour</a></div>
</div><!-- /grid-b -->
            </div>
            
        </div>
        <!--Page affichage vente-->
        <div data-role="page" id="affichageVente">
            <div data-role="header">
                <a href="#gestionVente" data-icon="back" class="back ui-btn-left">Back</a>
                <h1 id='titreAffichageVente'></h1>
            </div>

            <div data-role="content" id='contenuVente'>
                
            </div>
            
        </div>
        <!----------------------->
        <!---Page consultation--->
        <!----------------------->
        <div data-role="page" id="consultation">
            <div data-role="header">
                <a href="#home" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Consultation</h1>
            </div>

            <div data-role="content">
                  <ul data-role="listview"  data-inset="true">
                    <li><a href="#compositionMet">Consulter la composition d'un met</a></li>
                    <li><a href="#consultationPrixVente">Consultation des prix de ventes</a></li>
                </ul>
            </div>
        </div>
        <!---Page consultation de composition des met (affichage de tous les mets) --->

        <div data-role="page" id="compositionMet">
            <div data-role="header">
                <a href="#consultation" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Consultation de la composition d'un met </h1>
            </div>
            <div data-role="content">
                 <?php foreach ($compoMet as $met) { ?>
                    <div class="ui-grid-b" id="div-<?php echo $met->id ?>">

                        <a href="#compositionMetClick" data-role="button" class="modificationMet"><?php echo $met->nom ?></a>
                    </div>
<?php } ?>

            </div>
        </div>

        <!---Page consultation de composition d'un met --->

        <div data-role="page" id="compositionMetClick">
            <div data-role="header" id='headerConsultationMet'>
                <a href="#compositionMet" data-icon="back" class="back ui-btn-left">Back</a>
                <h1 id='h1CompoMet'></h1>
            </div>
            <div data-role="content" id='contentCompoMet'>
                <table data-role="table" id="tableCompoMet" data-mode="reflow" class="ui-responsive">
                    <thead>
                        <tr>
                            <th>Ingrédient</th>
                            <th>Quantité</th>
                            <th>Unité</th>
                            <th>Prix</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                <center><p id="showPrixTotal"></p></center>
                
            </div>
        </div>

        <!--Ajout du fichier contenant le code JS que j'ai créer--> 
        <script src="js/codeRestau.js"></script>
    </body>
</html>
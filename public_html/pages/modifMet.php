<?php
include'../connexion.php';
$lesMets = $db->query('SELECT id, nom from met');
$lesIngrédient = $db->query('SELECT libelle FROM ingredient');
$lesPrix = $db->query('select met.nom as met, prixvente.prixDeVente as prix from prixvente join met on met.id = prixvente.idMet');
?>


<!DOCTYPE html>
<html class="ui-mobile-rendering">
    <head>
        <title>Restaurant Bopome</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <!-- The css -->
        <link rel="stylesheet" href="lib/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css"/>
        <link rel="stylesheet" href="css/css-bopome.css"/>
        <!-- The Scripts -->
        <script src="lib/jquery-1.11.3.min.js"></script>
        <script src="lib/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
    </head>

    <body>
        <!--Page modification d'un met-->
        <div data-role="page" id="modifMet">
            <div data-role="header">
                <a href="#mets" data-icon="back" class="back ui-btn-left">Back</a>
                <h1>Modification d'un met</h1>
            </div>
            <div data-role="content">
                <!--php-->
                <?php
                $leMet = $db->query('SELECT * FROM met WHERE id = ' . $_GET['id']);
                $met = $leMet->fetch();
                ?>
                <!---->
                <form method="post" action="scriptPHP/modif.php" id='modifFormMet'  data-ajax="false">
                    <label for="nomMet">Nom du met :</label>
                    <input type="text" name="nomMet" id="nomMet" value="<?php echo $met->nom ?>">
                    <label for="nomMet">Description du met :</label>
                    <input type="text" name="descriptionMet" id="descriptionMet">
                    <label for="comMet">Commentaire :</label>
                    <input type="text" name="comMet" id="comMet">
                    <label for="selectType">Type du met :</label>
                    <select name="typeMet" id="select-native-1">
                        <option value='-1'></option>
                        <?php foreach ($lesTypesMets as $type) { ?>
                                                  <option value="<?php echo $type->id ?>"><?php echo $type->libelle ?></option>
                        <?php } ?>
                                          </select>
                    <input type='text' name='idMet' hidden>
                    <input type='submit' name='modifMet' value="Modifier">
                    <div data-role="popup" id="myPopup">
                        <p id='pModifMet'></p>
                    </div>
                </form>
            </div>
        </div>
        
    </body>
</html>
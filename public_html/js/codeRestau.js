//Permet la suppression d'un mets
$('.suppMet').on('click', function () {
    var idMet = {
        idMet: $(this).attr('id')
    };
    $.ajax({
        url: 'scriptPHP/delete.php',
        type: 'POST',
        dataType: 'json',
        data: idMet,
        success: function (data) {
            if (data.retour === true) {
                $('#div-' + data.id).next().remove();
                $('#div-' + data.id).fadeOut();
                $('#div-' + data.id).remove();
                $('#lienPopupSuppMet').trigger('click');
            }
        }
    });
});
//Partie concernant la modification d'un met
//On initialise un objet pour échanger des valeurs entre les multi-pages
//Objet contenant l'id du met à modifier
var idModifMet = {
    id: ''
};
$('#mets [href="#modifMet"]').on('click', function () {
    idModifMet.id = $(this).parent().parent().attr('id');
    idModifMet.id = idModifMet.id.replace('div-', "");
    $.post('scriptPHP/modif.php', 'idMet=' + idModifMet.id, function (donnee) {
        var data = $.parseJSON(donnee);
        $('#nomModifMet').val(data.nom);
        $('#descriptionModifMet').val(data.description);
        $('#comModifMet').val(data.commentaire);
        $('#selectModifMet').prop('selected', data.idTypeMet);
        $('#modifIdMet').val(data.id);
        $('#modifIdMet').hide();
    });
});
//Fais des vérification lors de la modification d'un met
$('#submitModifMet').on('click', function (e) {
    if ($('#selectModifMet').val() === "-1") {
        $('#pModifMet').text("Veuillez selectionner un type de met").css("font-size", 25).css('color', "red");
        $('#lienPopupModifMet').trigger('click');
        e.preventDefault();
    }
    if ($('#nomModifMet').val() === "") {
        $('#pModifMet').text("Veuillez saisir un nom de met").css("font-size", 25).css('color', "red");
        $('#lienPopupModifMet').trigger('click');
        e.preventDefault();
    }
    if ($('#descriptionModifMet').val() === "") {
        $('#pModifMet').text("Veuillez saisir une description de met").css("font-size", 25).css('color', "red");
        $('#lienPopupModifMet').trigger('click');
        e.preventDefault();
    }
});
$(document).load(function () {
    if ($('#lienPopupModifAccueilMet').length === 1) {
        $('#pModifAccueilMet').text("Met modifié").css("font-size", 25).css('color', "red");
        $('a [href="lienPopupModifAccueilMet"]').delay(1000).trigger('click');
    }
    ;
});
//Affichage de la composition d'un met
$('#compositionMet').on('click', 'a', function () {
    idModifMet.id = $(this).parent().attr('id');
    idModifMet.id = idModifMet.id.replace('div-', "");
    $.post('scriptPHP/consultation.php', 'idConsultationMet=' + idModifMet.id, function (donnee) {
        $('tbody > tr').remove();
        $('tfoot > tr').remove();
        var data = $.parseJSON(donnee);
        $('#h1CompoMet').text("Composition du met '" + data[0].Met + "'");
        var ingrédient = [];
        var qte = [];
        var prix = [];
        var unite = [];
        var prixtotal = 0;
        for (i = 0; i < data.length; i++) {
            ingrédient.push(data[i].Ingrédient);
            qte.push(data[i].quantite);
            prix.push(data[i].prix);
            unite.push(data[i].unite);
        }
        ;
        for (var i = 0; i < prix.length; i++) {
            prixtotal += parseFloat(prix[i]);
        }
        ;
        prixtotal = Math.round(prixtotal * 100) / 100;


        $.each(ingrédient, function (index, valeur) {
            $('#tableCompoMet tbody').append('<tr><td>' + valeur + '</td><td>' + qte[index] + '</td><td>' + unite[index] + '</td><td>' + prix[index] + ' €</td></tr>');
        });
        $('#showPrixTotal').html("Prix total : ");
        $('#showPrixTotal').append(prixtotal + ' €')
        $('#showPrixTotal').css('color', 'red').css('font-size', '16px');
    });
});
//Partie concernant la modification d'un ingrédient
//On initialise un objet pour échanger des valeurs entre les multi-pages
//Objet contenant l'id du met à modifier
var idModifIngredient = {
    id: ''
};
$('#ingredient [href="#modifIngredient"]').on('click', function () {
    idModifIngredient.id = $(this).parent().parent().attr('id');
    idModifIngredient.id = idModifIngredient.id.replace('div-', "");
    $.post('scriptPHP/modif.php', 'idIngredient=' + idModifIngredient.id, function (donnee) {
        var data = $.parseJSON(donnee);
        $('#libelleModifIngredient').val(data.libelle);
        $('#modifNbUnite').val(data.nombreUnite);
        $('#modifPrixUnite').val(data.prixNombreUnite);
        $('#modifTypeUnite').prop('selected', data.idTypeUnite);
        $('#modifIdIngredient').val(data.id);
        $('#modifIdIngredient').hide();
    });
});
$('[href="#affichageVente"] ').on('click', function () {
    $('#contenuVente').html('');
    var d = new Date();
    if ($(this).attr('id') === 'ddj') {
        var dateDuJour = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
        $.post('scriptPHP/consultation.php', 'date=' + dateDuJour, function (donnee) {
            var data = $.parseJSON(donnee);
            $.each(data, function (index, valeur) {
                $('#contenuVente').append('<div id="vente-' + data[index].idMet + '"></div>');
                $('#vente-' + data[index].idMet).css('border', '1px solid black');
                $('#vente-' + data[index].idMet).append('<button>' + data[index].nom + '</button>');
                $('#vente-' + data[index].idMet).append('<div class="ui-grid-b"></div>');
                $('#vente-' + data[index].idMet + ' > div').append('<button>' + data[index].nbVente + '</button>').append('<button>+</button>').append('<button>-</button>');
            });
        });
    } else if ($(this).attr('id') !== 'ddj') {
        var dateMoins = $(this).attr('id').replace('ddj-', "");
        var dateDuJour = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + (d.getDate() - dateMoins);
        $.post('scriptPHP/consultation.php', 'date=' + dateDuJour, function (donnee) {
            var data = $.parseJSON(donnee);
            $.each(data, function (index, valeur) {
                $('#contenuVente').append('<div id="vente-' + data[index].idMet + '"></div>');
                $('#vente-' + data[index].idMet).append('<button>' + data[index].nom + '</button>');
                $('#vente-' + data[index].idMet).append('<div data-role="controlgroup"></div>');
                $('#vente-' + data[index].idMet + ' > div').append('<button>' + data[index].nbVente + '</button>').append('<button>+</button>').append('<button>-</button>');
            });
        });
    }
    $('#titreAffichageVente').html('Vente du ' + dateDuJour);
});
//Concerne la partie de modification de la composition d'un met
var idCompoMet = {
    id: ''
};
$('[href="#modifCompoMet"]').on('click', function () {
    idCompoMet.id = $(this).parent().parent().attr('id');
    idCompoMet.id = idCompoMet.id.replace('div-', "");
    $.post('scriptPHP/consultation.php', 'idConsultationMet=' + idCompoMet.id, function (donnee) {
        $('tbody > tr').remove();
        var data = $.parseJSON(donnee);
        $('#h1compoModifMet').html("Composition du met '" + data[0].Met + "'");
        var ingrédient = [];
        var idIngre = [];
        var qte = [];
        var unite = [];
        for (i = 0; i < data.length; i++) {
            ingrédient.push(data[i].Ingrédient);
            idIngre.push(data[i].idIngre);
            qte.push(data[i].quantite);
            unite.push(data[i].unite);
        }
        ;
        $.each(ingrédient, function (index, valeur) {
            $('#tableCompoModifMet tbody').append('<tr><td>' + valeur + '</td><td>' + qte[index] + '</td><td>' + unite[index] + '</td></tr>');
        });
    });
});
$('.ajoutIngreCompo').on('click', function () {
    $.post('scriptPHP/consultation.php', 'consultationIngredient=' + 1, function (donnee) {
        var data = $.parseJSON(donnee);
        $('#contentCompoModifMet').append('<label for="selectModifCompoMet" class="select">Selectionnez l\'ingrédient à ajouter</label>');
        $('#contentCompoModifMet').append('<select name="selectModifCompoMet" id="selectModifCompoMet"><option value="-1"></option></select>');
        $.each(data, function (i) {
            $('#selectModifCompoMet').append('<option value="' + data[i].idIngredient + '"> ' + data[i].nom  + ' ('+data[i].unite + ')</option>');
        });
    });
});
$('#modifCompoMet').on('change','#selectModifCompoMet',function(){
    if ($('#qteModifCompo').length === 0 ) {
        $('#contentCompoModifMet').append('<label for="qteModifCompo">Quantité :</label><input name="qteModifCompo" id="qteModifCompo" value="" type="text">');
        $('#contentCompoModifMet').append('<button id="btnModifCompo">Envoyer</button>');
    }
});
$('#modifCompoMet').on('click','#btnModifCompo', function(){
    var ingredient = $('#selectModifCompoMet').val();
    var met = idCompoMet.id;
    var qte = $('#qteModifCompo').val();
    $.post('scriptPHP/modif.php', 'modifCompoMet=' + 1 + '&idIngreCompo=' + ingredient + "&idMetCompo=" + met + "&qte=" + qte, function (donnee) {
        var data = $.parseJSON(donnee);
       
    });
});


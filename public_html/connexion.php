<?php
//Nom de l'ip de la base de données
$server="localhost";
//Nom de l'utilisateur
$user="root";
//Mot de passe de connexion
$password="";
//Création de la variables db pour se connecter à la base
try
{
    $db= new PDO('mysql:host=localhost;dbname=restaurant_bopome;charset=UTF8',$user, $password);
}
catch(Exception $e)
{
    echo "Erreur PDO : " . $e;
}
//Permet de récuperer les erreurs
$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
//Tout les enregistrement seront des objets
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
?>
